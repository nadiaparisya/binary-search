#include <stdio.h>  
int binarySearch(int a[], int awal, int akhir, int val)    
    {    
    int tengah;    
    if(akhir >= awal)     
     {        tengah = (awal + akhir)/2;    

        if(a[tengah] == val)    
        {                 
            return tengah+1;    
        }    
            else if(a[tengah] < val)     
        {  
            return binarySearch(a, tengah+1, akhir, val);    
        }    
            else     
        {  
            return binarySearch(a, awal, tengah-1, val);    
            }          
        }    
        return -1;     
}   
int main() {  
    int a[] = {10, 12, 24, 32, 46, 48, 52, 60, 70};
    int val = 52; 
    int n = sizeof(a) / sizeof(a[0]);
    int res = binarySearch(a, 0, n-1, val);
    printf("Elemen Array : ");  
    for (int i = 0; i < n; i++)  
    printf("%d ", a[i]);   
    printf("\nElemen yang dicari : %d", val);  
    if (res == -1)  
    printf("\nElemen tidak terdapat pada array");  
    else  
    printf("\nElemen muncul pada posisi %d", res);  
    return 0;  
} 